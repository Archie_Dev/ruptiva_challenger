# frozen_string_literal: true

Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :api do
    namespace :v1 do
      # Users Routes
      get '/users', to: 'users#show'
      put '/users/:id', to: 'users#update'
      delete '/users/:id', to: 'users#destroy'
      # Adm Routes
      get '/users/admin', to: 'users#adm_show'
      put '/users/admin/:id', to: 'users#adm_update'
      delete '/users/admin/:id', to: 'users#adm_destroy'
    end
  end
end
