# Update Current User

Allow the Authenticated User to update their details.

**URL** : `/api/v1/users/:id`

**Method** : `PUT`

**Auth required** : YES

**Permissions required** : None

**Data constraints**

```json
{
    "first_name": "[require]",
    "last_name": "[require]",
    "email": "[require valid email address]",
    "password": "[require]",
}
```
**Data examples**

```json
{
    "first_name": "John"
}
```
## Success Responses

**Condition** : Data provided is valid and User is Authenticated.

**Code** : `204 no content`
## Error Response

**Condition** : Unauthenticated User

**Code** : `401 Unauthorized`
