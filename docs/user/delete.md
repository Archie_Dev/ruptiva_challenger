# Update Current User

Allow the Authenticated User to delete their details.

**URL** : `/api/v1/users/:id`

**Method** : `DELETE`

**Auth required** : YES

## Success Responses

**Condition** : User is Authenticated.

**Code** : `204 no content`
## Error Response

**Condition** : Unauthenticated User

**Code** : `401 Unauthorized`
