# Show Current User

Get the details of the currently Authenticated User along with basic
subscription information.

**URL** : `/api/v1/users`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

Return current User.

```json
{
    "id": 1,
    "provider": "email",
    "uid": "jon@email.com",
    "allow_password_change": false,
    "first_name": "Jon",
    "last_name": "Doe",
    "email": "jon@email.com",
    "role": "standard",
}
```

## Error Response

**Condition** : If the user is not logged in.

**Code** : `401 Unauthorized`

## Notes
**Header example**

```json
{
    "access-token": "8ylymXo0Fem8gJr2ANYyyw",
    "client": "DsYZE6Vwq_prJVDTE7--xQ",
    "uid": "joao@email.com"
}
```

