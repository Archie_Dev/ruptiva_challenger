# Create

Used to create User.

**URL** : `/auth`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "first_name": "[require]",
    "last_name": "[require]",
    "email": "[require valid email address]",
    "password": "[require]",
    "password_confirmation": "[require]"
}
```

**Data example**

```json
{
  "first_name": "João",
  "last_name": "da Silva",
  "email": "joao@email.com",
  "password": "12345678",
  "password_confirmation": "12345678"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Condition** : If 'email' already been taken.

**Code** : `422 Unprocessable Entity`
