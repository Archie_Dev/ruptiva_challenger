# Sign_in

Used to authenticate User.

**URL** : `/auth/sign_in`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "email": "[require user email]",
    "password": "[require user password]",
}
```

**Data example**

```json
{
  "email": "joao@email.com",
  "password": "12345678"
}
```

## Success Response

**Code** : `200 OK`

**Header example**

```json
{
    "access-token": "8ylymXo0Fem8gJr2ANYyyw",
    "client": "DsYZE6Vwq_prJVDTE7--xQ",
    "uid": "joao@email.com"
}
```

## Error Response

**Condition** : If 'email'/'password' invalid.

**Code** : `401 Unauthorized`
