# Show All Users

Get the details of the all Users along with basic
subscription information.

**URL** : `/api/v1/users/admin`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : Admin role

## Success Response

**Code** : `200 OK`

**Content examples**

Return All Users.

```json
{
    "id": 1,
    "provider": "email",
    "uid": "jon@email.com",
    "allow_password_change": false,
    "first_name": "Jon",
    "last_name": "Doe",
    "email": "jon@email.com",
    "role": "standard",

    "id": 2,
    "provider": "email",
    "uid": "joana@email.com",
    "allow_password_change": false,
    "first_name": "Joana",
    "last_name": "Doe",
    "email": "joana@email.com",
    "role": "admin",
}
```

## Error Response

**Condition** : If the user is not logged in/or not adm.

**Code** : `401 Unauthorized`

## Notes
**Header example**

```json
{
    "access-token": "8ylymXo0Fem8gJr2ANYyyw",
    "client": "DsYZE6Vwq_prJVDTE7--xQ",
    "uid": "joao@email.com"
}
```
