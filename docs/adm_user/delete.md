# Delete Users

Allow the Adm User to delete details.

**URL** : `/api/v1/users/:id`

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : Admin role

## Success Responses

**Condition** : User is Authenticated and is Admin.

**Code** : `204 no content`
## Error Response

**Condition** : Unauthenticated User

**Code** : `401 Unauthorized`

