# Update Users

Allow the adm User to update details.

**URL** : `/api/v1/users/:id`

**Method** : `PUT`

**Auth required** : YES

**Permissions required** : Admin Role

**Data constraints**

```json
{
    "first_name": "[require]",
    "last_name": "[require]",
    "email": "[require valid email address]",
    "password": "[require]",
}
```
**Data examples**

```json
{
    "first_name": "John"
}
```
## Success Responses

**Condition** : Data provided is valid, User is Authenticated and is admin.

**Code** : `204 no content`

## Error Response

**Condition** : Unauthenticated User

**Code** : `401 Unauthorized`
