# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def adm_show?
    user.admin?
  end

  def adm_update?
    user.admin?
  end

  def adm_destroy?
    user.admin
  end
end
