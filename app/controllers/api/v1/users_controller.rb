# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApplicationController
      before_action :set_user, only: %i[adm_update adm_destroy update destroy]

      before_action :authenticate_user!
      # GET /api/v1/users
      def show
        @user = current_user
        json_response(@user)
      end

      # GET /api/v1/users/admin
      def adm_show
        @users = User.all
        authorize @users
        json_response(@users)
      end

      # PUT /api/v1/users
      def update
        if current_user.id == params[:id]
          @user.update(user_params)
          head :no_content
        end
      end

      # PUT /api/v1/users/admin/:id
      def adm_update
        @user.update(user_params)
        authorize @user
        head :no_content
      end

      def destroy
        if current_user.id == params[:id]
          @user.destroy
          head :no_content
        end
      end

      def adm_destroy
        @user.destroy
        authorize @user
        head :no_content
      end

      private

      def user_params
        # whitelist params
        params.permit(:first_name, :last_name, :email)
      end

      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end
