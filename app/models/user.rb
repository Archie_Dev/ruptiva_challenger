# frozen_string_literal: true

class User < ActiveRecord::Base
  extend Devise::Models

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # name_of_person setup
  has_person_name

  # Enum Role: 0 = standard 1 = admin
  enum role: %i[standard admin]

  # Validations
  validates :first_name,
            :last_name,
            :email,
            :password,
            :password_confirmation,
            presence: true,
            on: :create

  include DeviseTokenAuth::Concerns::User
end
