# frozen_string_literal: true

# This will guess the User class
FactoryBot.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email      { Faker::Internet.email }
    password   { 'foobar' }
    password_confirmation { 'foobar' }
  end
end
