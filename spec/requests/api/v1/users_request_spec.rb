# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Api::V1::Users', type: :request do
  # Test suite for GET /users
  # Create User
  let(:user) { create(:user) }
  let(:admin) { create(:user, :admin) }

  # Set valid user headers
  let(:valid_headers) { user.create_new_auth_token }

  # Set invalid headers
  let(:invalid_headers) { { 'Authorization' => nil } }

  describe 'GET /users' do
    context 'VALID REQUEST when not logged as admin' do
      # make HTTP get request before each example
      before do
        get '/api/v1/users',
            params: {},
            headers: {
              'CONTENT_TYPE' => 'application/json',
              'ACCEPT' => 'application/json',
              'Uid' => valid_headers['uid'],
              'Access-Token' => valid_headers['access-token'],
              'Client' => valid_headers['client']
            }
      end

      it 'return user' do
        # Note `json` is a cutom helper to parse JSON responses
        expect(json).not_to be_empty
        expect(json['id']).to match(1)
      end

      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  context 'INVALID REQUEST when not logged as admin' do
    # make HTTP get request before each example
    before { get '/api/v1/users', params: {}, headers: invalid_headers }

    it 'not return user' do
      expect(json).not_to be_empty
      expect(json['errors']).to match(['You need to sign in or sign up before continuing.'])
    end

    it 'return status code 401' do
      expect(response).to have_http_status(401)
    end
  end

  # Test suite for PUT /tools
  describe 'PUT api/v1/users' do
    let(:valid_attributes) { { first_name: 'Wilson' }.to_json }

    context 'when the record exists' do
      before { put "/api/v1/users/#{user.id}", params: valid_attributes, headers: valid_headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE api/v1/tools/
  describe 'DELETE /users' do
    before { delete "/api/v1/users/#{user.id}", params: {}, headers: valid_headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
