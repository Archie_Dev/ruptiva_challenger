# frozen_string_literal: true

require 'rails_helper'

# Test suite for User model
RSpec.describe User, type: :model do
  # Validation test
  # ensure first_name, last_name, email, password, password_confirmation
  # and role
  # it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_presence_of(:password_confirmation) }
end
