# README

Api developed for the Ruptiva backend challenge.

## Ruby Version
2.7.2

## Starting the project
Both postgres and the API are in the docker-compose, just follow the instructions below to be able to run the app.

```sh
docker-compose build
```

```sh
docker-compose run web bundle exec rails db:create
```

```sh
docker-compose run web bundle exec rails db:migrate
```

```sh
docker-compose run web bundle exec rails db:seed
```

```sh
docker-compose up --build
```

The API is on port 3000

## Test
To run the tests just run the following command
```sh
docker-compose run web bundle exec rspec
```

## Docs
## Open Endpoints

Open endpoints require no Authentication.

* [Create](docs/create.md) : `POST /auth`
* [Sign_in](docs/sign_in.md) : `POST /auth/sign_in`

## Endpoints that require Authentication

Closed endpoints require a valid Token to be included in the header of the
request. A Token can be acquired from the sign_in view above.

### Normal User
Each endpoint manipulates or displays information related to the User whose
Token is provided with the request:

* [Show info](docs/user/get.md) : `GET /api/v1/users`
* [Update info](docs/user/put.md) : `PUT /api/v1/users/:id`
* [Delete info](docs/user/delete.md) : `DELETE /api/v1/users/:id`


### Adm User
Each endpoint manipulates or displays information related to the User whose
Token is provided with the request(The admin user is created automatically by the seed.Visit db/seeds.rb):

* [Show info](docs/adm_user/get.md) : `GET /api/v1/users/admin`
* [Update info](docs/adm_user/put.md) : `PUT /api/v1/users/admin/:id`
* [Delete info](docs/adm_user/delete.md) : `DELETE /api/v1/users/admin/:id`


